
package ua.nure.it.social.service;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ua.nure.it.social.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DAOException_QNAME = new QName("http://it.nure.ua/social/service", "DAOException");
    private final static QName _AddPost_QNAME = new QName("http://it.nure.ua/social/service", "addPost");
    private final static QName _AddPostResponse_QNAME = new QName("http://it.nure.ua/social/service", "addPostResponse");
    private final static QName _ClientToken_QNAME = new QName("http://it.nure.ua/social/service", "clientToken");
    private final static QName _DeletePost_QNAME = new QName("http://it.nure.ua/social/service", "deletePost");
    private final static QName _DeletePostResponse_QNAME = new QName("http://it.nure.ua/social/service", "deletePostResponse");
    private final static QName _EditPost_QNAME = new QName("http://it.nure.ua/social/service", "editPost");
    private final static QName _EditPostResponse_QNAME = new QName("http://it.nure.ua/social/service", "editPostResponse");
    private final static QName _FindByTitle_QNAME = new QName("http://it.nure.ua/social/service", "findByTitle");
    private final static QName _FindByTitleResponse_QNAME = new QName("http://it.nure.ua/social/service", "findByTitleResponse");
    private final static QName _GetPost_QNAME = new QName("http://it.nure.ua/social/service", "getPost");
    private final static QName _GetPostResponse_QNAME = new QName("http://it.nure.ua/social/service", "getPostResponse");
    private final static QName _NewPostsList_QNAME = new QName("http://it.nure.ua/social/service", "newPostsList");
    private final static QName _NewPostsListResponse_QNAME = new QName("http://it.nure.ua/social/service", "newPostsListResponse");
    private final static QName _ServerToken_QNAME = new QName("http://it.nure.ua/social/service", "serverToken");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ua.nure.it.social.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DAOException }
     * 
     */
    public DAOException createDAOException() {
        return new DAOException();
    }

    /**
     * Create an instance of {@link AddPost }
     * 
     */
    public AddPost createAddPost() {
        return new AddPost();
    }

    /**
     * Create an instance of {@link AddPostResponse }
     * 
     */
    public AddPostResponse createAddPostResponse() {
        return new AddPostResponse();
    }

    /**
     * Create an instance of {@link DeletePost }
     * 
     */
    public DeletePost createDeletePost() {
        return new DeletePost();
    }

    /**
     * Create an instance of {@link DeletePostResponse }
     * 
     */
    public DeletePostResponse createDeletePostResponse() {
        return new DeletePostResponse();
    }

    /**
     * Create an instance of {@link EditPost }
     * 
     */
    public EditPost createEditPost() {
        return new EditPost();
    }

    /**
     * Create an instance of {@link EditPostResponse }
     * 
     */
    public EditPostResponse createEditPostResponse() {
        return new EditPostResponse();
    }

    /**
     * Create an instance of {@link FindByTitle }
     * 
     */
    public FindByTitle createFindByTitle() {
        return new FindByTitle();
    }

    /**
     * Create an instance of {@link FindByTitleResponse }
     * 
     */
    public FindByTitleResponse createFindByTitleResponse() {
        return new FindByTitleResponse();
    }

    /**
     * Create an instance of {@link GetPost }
     * 
     */
    public GetPost createGetPost() {
        return new GetPost();
    }

    /**
     * Create an instance of {@link GetPostResponse }
     * 
     */
    public GetPostResponse createGetPostResponse() {
        return new GetPostResponse();
    }

    /**
     * Create an instance of {@link NewPostsList }
     * 
     */
    public NewPostsList createNewPostsList() {
        return new NewPostsList();
    }

    /**
     * Create an instance of {@link NewPostsListResponse }
     * 
     */
    public NewPostsListResponse createNewPostsListResponse() {
        return new NewPostsListResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DAOException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DAOException }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "DAOException")
    public JAXBElement<DAOException> createDAOException(DAOException value) {
        return new JAXBElement<DAOException>(_DAOException_QNAME, DAOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPost }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddPost }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "addPost")
    public JAXBElement<AddPost> createAddPost(AddPost value) {
        return new JAXBElement<AddPost>(_AddPost_QNAME, AddPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPostResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddPostResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "addPostResponse")
    public JAXBElement<AddPostResponse> createAddPostResponse(AddPostResponse value) {
        return new JAXBElement<AddPostResponse>(_AddPostResponse_QNAME, AddPostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "clientToken")
    public JAXBElement<String> createClientToken(String value) {
        return new JAXBElement<String>(_ClientToken_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePost }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeletePost }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "deletePost")
    public JAXBElement<DeletePost> createDeletePost(DeletePost value) {
        return new JAXBElement<DeletePost>(_DeletePost_QNAME, DeletePost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePostResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeletePostResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "deletePostResponse")
    public JAXBElement<DeletePostResponse> createDeletePostResponse(DeletePostResponse value) {
        return new JAXBElement<DeletePostResponse>(_DeletePostResponse_QNAME, DeletePostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditPost }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditPost }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "editPost")
    public JAXBElement<EditPost> createEditPost(EditPost value) {
        return new JAXBElement<EditPost>(_EditPost_QNAME, EditPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditPostResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditPostResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "editPostResponse")
    public JAXBElement<EditPostResponse> createEditPostResponse(EditPostResponse value) {
        return new JAXBElement<EditPostResponse>(_EditPostResponse_QNAME, EditPostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByTitle }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByTitle }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "findByTitle")
    public JAXBElement<FindByTitle> createFindByTitle(FindByTitle value) {
        return new JAXBElement<FindByTitle>(_FindByTitle_QNAME, FindByTitle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByTitleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByTitleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "findByTitleResponse")
    public JAXBElement<FindByTitleResponse> createFindByTitleResponse(FindByTitleResponse value) {
        return new JAXBElement<FindByTitleResponse>(_FindByTitleResponse_QNAME, FindByTitleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPost }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPost }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "getPost")
    public JAXBElement<GetPost> createGetPost(GetPost value) {
        return new JAXBElement<GetPost>(_GetPost_QNAME, GetPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPostResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPostResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "getPostResponse")
    public JAXBElement<GetPostResponse> createGetPostResponse(GetPostResponse value) {
        return new JAXBElement<GetPostResponse>(_GetPostResponse_QNAME, GetPostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewPostsList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NewPostsList }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "newPostsList")
    public JAXBElement<NewPostsList> createNewPostsList(NewPostsList value) {
        return new JAXBElement<NewPostsList>(_NewPostsList_QNAME, NewPostsList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewPostsListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NewPostsListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "newPostsListResponse")
    public JAXBElement<NewPostsListResponse> createNewPostsListResponse(NewPostsListResponse value) {
        return new JAXBElement<NewPostsListResponse>(_NewPostsListResponse_QNAME, NewPostsListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://it.nure.ua/social/service", name = "serverToken")
    public JAXBElement<String> createServerToken(String value) {
        return new JAXBElement<String>(_ServerToken_QNAME, String.class, null, value);
    }

}
