package ua.nure.holub.lab3;

import ua.nure.it.social.*;
import ua.nure.it.social.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final String CLIENT_TOKEN_VALID = "clientToken";
    private static String CLIENT_TOKEN = "clientToken";
    private static void printMenu() {
        System.out.println("1. Get posts\n" +
                "2. Get post by id\n" +
                "3. Add post\n" +
                "4. Update post\n" +
                "5. Delete post\n" +
                "6. Find by header\n" +
                "7. Exit\n" +
                "8. Send with invalid token\n" +
                "9. Send with valid token");
    }

    private static void getPosts(PostService service) {
        var raw = service.newPostsList(new NewPostsList(), CLIENT_TOKEN, null).getReturn();
        System.out.println("Got response " + raw.toString());
        for (Post post : raw.getPost()) {
            System.out.println(post.toString());
        }
    }

    private static void getPostById(PostService service, Scanner scanner) throws Exception {
        var params = new GetPost();
        System.out.println("Enter post id: ");
        int id = Integer.valueOf(scanner.nextLine());
        params.setId(id);
        var raw = service.getPost(params, CLIENT_TOKEN, null).getReturn();
        System.out.println("Got post " + raw.toString());
    }

    private static void findPostByHeader(PostService service, Scanner scanner) throws Exception {
        var params = new FindByTitle();
        System.out.println("Enter posts header: ");
        String header = scanner.nextLine();
        params.setPattern(header);
        var raw = service.findByTitle(params, CLIENT_TOKEN, null).getReturn();
        System.out.println("Got response " + raw.toString());
        for (Post post : raw.getPost()) {
            System.out.println(post.toString());
        }
    }

    private static void addPost(PostService service, Scanner scanner) throws DAOException_Exception {
        var params = new AddPost();
        var post = new Post();
        System.out.println("Enter post header: ");
        String header = scanner.nextLine();
        System.out.println("Enter post text: ");
        String text = scanner.nextLine();
        System.out.println("How many images you want to add: ");
        int imageCount = Integer.valueOf(scanner.nextLine());
        List<Image> imagesList = new ArrayList<>();
        for (int i = 0; i < imageCount; i++) {
            var image = new Image();
            System.out.println("Enter image URL: ");
            String url = scanner.nextLine();
            image.setImageUrl(url);
            image.setId(123);
            imagesList.add(image);
        }
        System.out.println("Enter your name: ");
        String username = scanner.nextLine();
        System.out.println("Select access level: \n" +
                "1. PRIVATE\n" +
                "2. PUBLIC\n" +
                "3. FRIEND ONLY");
        AccessLevel accessLevel = null;
        var selectFlag = true;
        while (selectFlag) {
            int choise = Integer.valueOf(scanner.nextLine());
            switch (choise) {
                case 1 -> {
                    accessLevel = AccessLevel.HIDDEN;
                    selectFlag = false;
                }
                case 2 -> {
                    accessLevel = AccessLevel.PUBLIC;
                    selectFlag = false;
                }
                case 3 -> {
                    accessLevel = AccessLevel.FRIENDS_ONLY;
                    selectFlag = false;
                }
                default -> {
                    System.out.println("Invalid option");
                    break;
                }
            }
        }
        post.setHeader(header);
        post.setText(text);
        var images = new Images();
        images.getImage().addAll(imagesList);
        post.setImages(images);
        post.setAccessLevel(accessLevel.value());
        var createdBy = new Author();
        createdBy.setUsername(username);
        createdBy.setId(123);
        var createdByImage = new Image();
        createdByImage.setImageUrl("http://test.png");
        createdBy.setImage(createdByImage);
        post.setCreatedBy(createdBy);
        params.setPost(post);
        var request = service.addPost(params, CLIENT_TOKEN, null);
        var res = request.getReturn();
        System.out.println("Created post with id " + res);
    }

    private static void updatePost(PostService service, Scanner scanner) throws DAOException_Exception {
        var params = new EditPost();
        System.out.println("Enter id of post to edit: ");
        int id = Integer.valueOf(scanner.nextLine());
        var post = new Post();
        System.out.println("Enter post header: ");
        String header = scanner.nextLine();
        System.out.println("Enter post text: ");
        String text = scanner.nextLine();
        System.out.println("How many images you want to add: ");
        int imageCount = Integer.valueOf(scanner.nextLine());
        List<Image> imagesList = new ArrayList<>();
        for (int i = 0; i < imageCount; i++) {
            var image = new Image();
            System.out.println("Enter image URL: ");
            String url = scanner.nextLine();
            image.setImageUrl(url);
            image.setId(123);
            imagesList.add(image);
        }
        System.out.println("Enter your name: ");
        String username = scanner.nextLine();
        System.out.println("Select access level: \n" +
                "1. PRIVATE\n" +
                "2. PUBLIC\n" +
                "3. FRIEND ONLY");
        AccessLevel accessLevel = null;
        var selectFlag = true;
        while (selectFlag) {
            int choise = Integer.valueOf(scanner.nextLine());
            switch (choise) {
                case 1 -> {
                    accessLevel = AccessLevel.HIDDEN;
                    selectFlag = false;
                }
                case 2 -> {
                    accessLevel = AccessLevel.PUBLIC;
                    selectFlag = false;
                }
                case 3 -> {
                    accessLevel = AccessLevel.FRIENDS_ONLY;
                    selectFlag = false;
                }
                default -> {
                    System.out.println("Invalid option");
                    break;
                }
            }
        }
        post.setHeader(header);
        post.setText(text);
        var images = new Images();
        images.getImage().addAll(imagesList);
        post.setImages(images);
        post.setAccessLevel(accessLevel.value());
        var createdBy = new Author();
        createdBy.setUsername(username);
        createdBy.setId(123);
        var createdByImage = new Image();
        createdByImage.setImageUrl("http://test.png");
        createdBy.setImage(createdByImage);
        post.setCreatedBy(createdBy);
        params.setPost(post);
        params.setId(id);
        var request = service.editPost(params, CLIENT_TOKEN, null);
        var res = request.getReturn();
        System.out.println("Updated post with id " + res);
    }

    private static void deletePost(PostService service, Scanner scanner) throws DAOException_Exception {
        var params = new DeletePost();
        System.out.println("Enter id of post to delete: ");
        int id = Integer.valueOf(scanner.nextLine());
        params.setId(id);
        var request = service.deletePost(params, CLIENT_TOKEN, null);
        var res = request.getReturn();
        System.out.println("Deleted post " + res.toString());
    }
    public static void main(String[] args) throws Exception {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        PostService client = new ua.nure.it.social.service.Posts().getPostPort();
        Scanner sc = new Scanner(System.in);
        printMenu();
        while (true) {
            int choise = Integer.valueOf(sc.nextLine());
            try {
                switch (choise) {
                    case 1 -> {
                        getPosts(client);
                        break;
                    }
                    case 2 -> {
                        getPostById(client, sc);
                        break;
                    }
                    case 3 -> {
                        addPost(client, sc);
                        break;
                    }
                    case 4 -> {
                        updatePost(client, sc);
                        break;
                    }
                    case 5 -> {
                        deletePost(client, sc);
                        break;
                    }
                    case 6 -> {
                        findPostByHeader(client, sc);
                        break;
                    }
                    case 7 -> {
                        return;
                    }
                    case 8 -> {
                        CLIENT_TOKEN = null;
                    }
                    case 9 -> {
                        CLIENT_TOKEN = CLIENT_TOKEN_VALID;
                    }
                    default -> {
                        System.out.println("Invalid option");
                        break;
                    }
                }
            } catch (Exception e) {
                System.out.println("[Error] " + e.getMessage());
                e.printStackTrace();
            }
            printMenu();
        }
    }
}
