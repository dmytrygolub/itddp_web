package ua.nure.order.server.service;

import jakarta.jws.HandlerChain;
import jakarta.jws.WebService;
import jakarta.xml.ws.Holder;
import ua.nure.order.entity.Post;
import ua.nure.order.entity.Posts;
import ua.nure.order.server.dao.PostDAO;
import ua.nure.order.server.dao.PostDAOInMemoryImpl;
import ua.nure.order.server.dao.DAOException;

@HandlerChain(file = "security_handler.xml")
@WebService(serviceName="Posts",
		portName="PostPort",
		endpointInterface= "ua.nure.order.server.service.PostService",
		targetNamespace="http://it.nure.ua/social/service")
public class PostServiceImpl implements PostService {
	private static final PostDAO postDao = PostDAOInMemoryImpl.instance();

	@Override
	public Posts findByTitle(String pattern, String clientToken, Holder<String> serverToken) {
		return postDao.findByTitle(pattern);
	}

	@Override
	public Post getPost(int id, String clientToken, Holder<String> serverToken) throws DAOException {
		return postDao.findById(id);
	}

	@Override
	public Posts newPostsList(String clientToken, Holder<String> serverToken) {
		return postDao.newPosts();
	}

	@Override
	public int addPost(Post Post, String clientToken, Holder<String> serverToken) throws DAOException {
		return postDao.addPost(Post);
	}

	@Override
	public Post deletePost(int id, String clientToken, Holder<String> serverToken)
			throws DAOException {
		return postDao.deletePost(id);
	}

	@Override
	public int editPost(int id, Post post, String clientToken, Holder<String> serverToken) throws DAOException {
		return postDao.editPost(id, post);
	}
}
