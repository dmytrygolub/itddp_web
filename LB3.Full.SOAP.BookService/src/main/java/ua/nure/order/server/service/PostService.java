package ua.nure.order.server.service;

import java.sql.SQLException;
import java.util.Collection;

import com.sun.istack.NotNull;
import jakarta.annotation.Nonnull;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebParam.Mode;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.xml.ws.Holder;
import ua.nure.order.entity.Post;
import ua.nure.order.entity.Posts;
import ua.nure.order.server.dao.DAOException;

@WebService(targetNamespace = Const.SERVICE_NS)
public interface PostService {

    @WebMethod()
    @WebResult(targetNamespace = "http://it.nure.ua/social")
    public Post getPost(
            @WebParam(name = "id")
            @NotNull
            int id,
            @WebParam(name = "clientToken", header = true)
            String clientToken,
            @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
            Holder<String> serverToken) throws DAOException;

    @WebMethod()
    public int addPost(
            @WebParam(name = "Post", targetNamespace = "http://it.nure.ua/social")
            @NotNull
            Post Post, @WebParam(name = "clientToken", header = true)
            String clientToken,
            @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
            Holder<String> serverToken) throws DAOException;

    @WebMethod()
    @WebResult(targetNamespace = "http://it.nure.ua/social")
    public Post deletePost(
            @WebParam(name = "id")
            @NotNull
            int id,
            @WebParam(name = "clientToken", header = true)
            String clientToken,
            @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
            Holder<String> serverToken
    ) throws DAOException;


    @WebMethod()
    @WebResult(targetNamespace = "http://it.nure.ua/social")
    public Posts newPostsList(@WebParam(name = "clientToken", header = true)
                                             String clientToken,
                              @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
                                             Holder<String> serverToken);

    @WebMethod()
    @WebResult(targetNamespace = "http://it.nure.ua/social")
    public Posts findByTitle(
            @WebParam(name = "pattern")
            @NotNull
            String pattern,
            @WebParam(name = "clientToken", header = true)
            String clientToken,
            @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
            Holder<String> serverToken);

    @WebMethod()
    @WebResult(targetNamespace = "http://it.nure.ua/social")
    int editPost(
            @WebParam(name = "id")
            @NotNull
            int id,
            @WebParam(name = "Post", targetNamespace = "http://it.nure.ua/social")
            @NotNull
            Post post,
            @WebParam(name = "clientToken", header = true)
            String clientToken,
            @WebParam(name = "serverToken", header = true, mode = Mode.OUT)
            Holder<String> serverToken) throws DAOException;
}