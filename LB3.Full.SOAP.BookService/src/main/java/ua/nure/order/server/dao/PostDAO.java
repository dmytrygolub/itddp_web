package ua.nure.order.server.dao;

import ua.nure.order.entity.Post;
import ua.nure.order.entity.Posts;

import java.util.Collection;

public interface PostDAO {
	public int addPost(Post item) throws DAOException;
	public Post deletePost(int id) throws DAOException;
	public Posts findByTitle(String pattern);
	public Posts newPosts();
	public Post findById(Integer id) throws DAOException;
	int editPost(int id, Post post) throws DAOException;
}
